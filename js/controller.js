/* 
	Controller.js for DMS Travel Assignment
	Author: Kevin Kirby
*/
var controller = {};
var face = {};
var appId = '848571491910399';
// Shorthand for $( document ).ready()

$(function(e){
	$.getScript('//connect.facebook.net/en_US/sdk.js#xfbml=1', function(){ 
		// This function is called after sdk.js has been loaded 
		FB.init({
			appId: appId,
			xfbml: true,
			version: 'v2.8' 
		});
	});
	$(document).on('click', '#Login', function() {
		face.getToken();
		$('#splash').css('display','block');
		$('#Logout').css('display','block');
		$('#Login').css('display','none');
		$('#album').css('display','block');
		var name = localStorage.getItem('name')
		$('#welcome').html(name);
	});
	var name = localStorage.getItem('name')
	$('#Logout').css('display','none');
	if(localStorage.getItem('token')) {
		$('#Login').css('display','none');
		$('#album').css('display','block');
		$('#Logout').css('display','block');
		$('#splash').css('display','block');
		$('#welcome').html(name);
	}
	$(document).on('click', '#Logout', function () {
		face.logout()
		localStorage.removeItem('name');
		localStorage.removeItem('token');
		window.location.href = "http://kevinrkirby.com/facebook/#/";
		$('#Logout').css('display','none');
	});
	lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true,
       'alwaysShowNavOnTouchDevices': true
    })
});

var app = angular.module("photoApp", ['ngRoute','ngAnimate','ui.bootstrap.modal']);

app.config(function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl : 'pages/splash.html',
		})
		.when('/thumbs', {
			templateUrl : 'pages/thumbs.html',
			controller : 'PhotoReady'
		})
		.when('/album', {
			templateUrl : 'pages/album.html',
			controller : 'album',
		});
});

app.controller('album', function($scope, albumService, $location, $timeout){
	$scope.description = function(desc)  {
		$scope.desc = desc;
		$timeout(function() {	// Allows time for the data to arrive from facebook
            $scope.$apply();
        }, 1000);
	};
	$scope.feed = function(message) {
		$scope.feed = message;
		$timeout(function() {	// Allows time for the data to arrive from facebook
            $scope.$apply();
        }, 1000);
	};
	$scope.callToAddImages = function(id, location){
		localStorage.setItem('albumId', id)
        albumService.displayImages(id, location);
    };
	$scope.albums = function(data) {
		$scope.albums = data;
		$timeout(function() {	// Allows time for the data to arrive from facebook
            $scope.$apply();
        }, 1000);
		$scope.$apply();
	};
	$scope.likesPage = function(likes, index) {
		$scope.likes = likes.data;
		$scope.showLikes = true;
		$scope.$apply();
	};
	$scope.CommentsPage = function(comments, index, id) {
		$scope.comments = comments.data;
		$scope.imgID = id;
		$scope.count = comments.count;
		$timeout(function() {	// Allows time for the data to arrive from facebook
            $scope.$apply();
        }, 1000);
	}
	$scope.go = function () {
		face.albums($scope.albums);
		face.desc($scope.description);
		face.feed($scope.feed);
	};

	$scope.likePhoto = function(id, response) {
		var success = response.success;
		if(response.success == true) {

			angular.element('#'+id+'').html("Liked").css("color", "blue");	
		}
		else {
			angular.element('#'+id+'').html("Like").css("color", "black");
		}
		//console.log(response);
	}

	$scope.likeClick = function(id) {
		face.likeImg(id, $scope.likePhoto)
	}
	face.albums($scope.albums);
	face.feed($scope.feed);
	face.desc($scope.description);
	face.callBack = $scope.go;

});
app.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
});
app.service('albumService', function() {
	var imageList = [];
	var displayImages = function(id,location) {
		imageList.length = 0;
		var obj = {
			id: id,
			loc: location
		};
		imageList.push(obj);
		localStorage.setItem("albumID", JSON.stringify(imageList))
		
	};

	var getImages = function(){
		imageList = JSON.parse(localStorage.getItem("albumID"))
		return imageList;
	};

	imageList.length = 0;

	return {
		displayImages: displayImages,
		getImages: getImages
	};
});

app.controller('PhotoReady', function($scope, albumService, $timeout){
	var obj = albumService.getImages();
	var id = obj[0].id
	var id2 = id
	$scope.loc = obj[0].loc

	$scope.photoReady = function (photos) {
		$scope.photos = photos;
		$timeout(function() {	// Allows time for the data to arrive from facebook
            $scope.$apply();
        }, 1000);
        $scope.$apply();
	}
	$scope.postComment = function (id) {
		console.log($scope.comment, id)
		face.postComment($scope.comment, id)
	}
	face.pagePhotos($scope.photoReady, id);

	$scope.likesPage = function(likes, index, id) {
		$scope.pic_id = id;
		$scope.likes = likes;

	}
	$scope.CommentsPage = function(comments, index, id) {
		console.log(comments, index, id)
		if(comments && id) {
			$scope.comments = comments.data;
			$scope.imgID = id;
			$scope.count = comments.count;
			$timeout(function() {	// Allows time for the data to arrive from facebook
            $scope.$apply();
        }, 1000);
		}
		else {
			$scope.imgID = comments;
		} 
			
	}
	$scope.likePhoto = function(id, response) {
		var success = response.success;
		if(response.success == true) {
			angular.element('#'+id+'').html("Liked").css("color", "blue");	
		}
		else {
			angular.element('#'+id+'').html("Like").css("color", "black");
		}
		console.log(response);
	}

	$scope.likeClick = function(id) {
		face.likeImg(id, $scope.likePhoto);
		face.pagePhotos($scope.photoReady, id2);
	}

});
app.directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                    scope.$apply(function(){
                        scope.$eval(attrs.ngEnter, {'event': event});
                    });
                    event.preventDefault();
                    event.target.value
                }
            });
        };
    });