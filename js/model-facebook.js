	/* 
		Model.js for DMS Travel Assignment
		Author: Kevin Kirby
	*/
	face.photos = new Array ();
	face.likes = new Array();
	face.id = new Array();
	face.message = new Array();
	var arr =  new Array();
	face.name = {};
	face.profile = {};
	face.access_token = "";
	face.photoData = {};
	face.APPID = "848571491910399";
	face.DMS = "815157038515764";
	redirectURL = 'http://kevinrkirby.com/facebook/#/';
	face.photoReadyCallBack;
	face.descCallBack;
	face.feedCallBack;
	face.likeImgCallBack;
	face.liked;
	var x = 0;

	
	face.getToken = function() {		// log user into facebook
		FB.login(function(response) {
		    if (response.authResponse) {
				// console.log('Welcome!  Fetching your information.... ');
				FB.api('/me', function(response) {
				var resp = FB.getAuthResponse();
				localStorage.setItem("token", resp.accessToken);		// save access_token to localStorage
				localStorage.setItem("ID", response.id);				// save user id to localStorage
				face.getUserPic(response);								// get the users picture
				var url = localStorage.getItem('profile');				// get the users picture url
				var html = '<img src="'+url+'" width="20px" height="20px"> Hi '+response.name+''
				localStorage.setItem('name', html);						// save the html to local storage incase of page refresh
				var name = localStorage.getItem('name')
				$('#welcome').html(name);					
				$('#logout').html("Logout");							
				//console.log('Good to see you, ' + response.name + '.');
				face.albums(face.callBack)
				var array = [4,10, 6,15];
				array.sort();
				console.log(array)
				});
		    } else {
		     	//console.log('User cancelled login or did not fully authorize.');
		    }
		}, {
		    scope: ['publish_actions', 'manage_pages', 'publish_pages'],
		    return_scopes: true
		});
		return false;
	},
	face.likeImg = function(id, callBack) {		// function to like or unlink an image
		face.likeImgCallBack = callBack;
		var token = localStorage.getItem("token");

		// if the image has already beeen liked previously call face.unlikeImg()
		if(face.liked == true) {		
			console.log("face.like")
			face.unlikeImg(id, callBack)
		}
		else {
			// post like to facebook for the selected image
			FB.api(		
			  '/'+id+'/likes',
			  'POST',
			  { access_token: token, "fields":"like","":""},
			  function(response) {
		  	      if (response && !response.error) {
		  	      	face.liked = true;
			        face.likeImgCallBack(id, response)
			      }     
			  }
			);				
		}
	},
	face.unlikeImg = function(id, callBack) {		// function to unlike an image
		face.likeImgCallBack = callBack;
		var token = localStorage.getItem("token")
		FB.api(
		  '/'+id+'/likes',
		  'DELETE',
		  { access_token: token, "fields":"like","":""},
		  function(response) {
	  	      if (response && !response.error) {
	  	      	response = false;
	  	      	face.liked = false;
		        face.likeImgCallBack(id, response)
		      }     
		  }
		);
	},
	face.desc = function(callback) {			// retrieve the page description from facebook
		face.descCallBack = callback;
		var token = localStorage.getItem("token")
		var desc = "https://graph.facebook.com/v2.8/"+face.DMS+"?fields=description& \
		access_token="+token+"";
		$.get(desc, function(response) { 
			face.descCallBack(response)
		}, "json");
	},

	face.getUserPic = function(data) {			// gets the users profile picture
		var token = localStorage.getItem('token');
		var pic = "https://graph.facebook.com/v2.8/"+data.id+"?fields=picture&\
			access_token="+token+"";
			$.get(pic, function(response) {
				localStorage.setItem('profile', response.picture.data.url)
			}, "json");
	},
	face.logout = function() {				// log the user out of facebook
		var token = localStorage.getItem('token')
	    window.location.replace('https://www.facebook.com/logout.php?access_token='+token+'&confirm=1&next='+redirectURL+'');
	},
	face.feed = function(callBack) {		// dispaly the pages visitor posts that have been liked by the page admin
		face.feedCallBack = callBack;
		var token = localStorage.getItem("token");
		var feed = "https://graph.facebook.com/v2.8/"+face.DMS+"?fields=feed%7Bmessage%2Cfrom%2Cstory%7D&\
		access_token="+token+"";
		$.get(feed, function(response) {
			for(var i in response.feed.data) {
				if(!response.feed.data[i].story) {
					// get user page visitor posts
					face.getFeedLikes(response.feed.data[i])
				}
			}
			face.feedCallBack(face.message)
		}, "json");
	},
	face.getFeedLikes = function(data) {		// get the likes for each visitor post
		var id = data.id
		var token = localStorage.getItem('token')
		var likes = "https://graph.facebook.com/v2.8/"+id+"?fields=likes&\
		access_token="+token+"";
		$.get(likes, function(response){
			try {
				for(var i in response.likes.data){
					// if the post has been liked by the page admin
					if(response.likes.data[i].id == ""+face.DMS+"") {
						face.getFromPic(data)
					}
				}
			}
			catch(err) {
			}
		}, "json");
	},
	face.getFromPic = function(data) {		// ge the profile picture of the person who posted the message
		var token = localStorage.getItem('token');
		var pic = "https://graph.facebook.com/v2.8/"+data.from.id+"?fields=picture&\
			access_token="+token+"";
			$.get(pic, function(response) {
				data.picture = response.picture.data.url
			}, "json");
		face.message.push(data)
	},
	face.albums = function(callBack) {		// get the pages albums the are australian
		face.albumReadyCallBack = callBack;
		var cover = {};
		var token = localStorage.getItem("token");
		var albums = "https://graph.facebook.com/v2.8/"+face.DMS+"?fields=albums%7Bphotos%2Cname%2Clocation%2Ccover_photo%2Clikes%7Bpic%2Cname%7D%7D& \
		access_token="+token+"";
		$.get(albums, function(response) {
			for(i in response.albums.data) {
				if(response.albums.data[i].location) {
					var loc = response.albums.data[i].location
					// if they are austrlian albums
					if(loc.includes("Australia")) {
						var photoObj = {};
						response.albums.data[i].comments = face.comments(photoObj, response.albums.data[i].id)
						response.albums.data[i].cover_photo = face.getCover(response.albums.data[i].cover_photo.id)
						response.albums.data[i].likes.data.length = response.albums.data[i].likes.data.length
						arr.push(response.albums.data[i])	
					}						
				}
			}
			face.myFunction(arr);
			face.albumReadyCallBack(arr)
		}, "json");
	},
	face.myFunction = function(arr) {	// sort the albums based on the number of likes for the album
	    arr.sort(function(a, b) {
	    	return b.likes.data.length - a.likes.data.length;
	    });
	},
	face.getCover = function(id) {		// get the albums cover photo
		var url = {};
		var token = localStorage.getItem("token")
		var images = "https://graph.facebook.com/v2.8/"+id+"?fields=images& \
		access_token="+token+"";
		$.get(images, function(response) {
			url.src = response.images[response.images.length - response.images.length].source;
		}, "json");
		return url
	},
	face.pagePhotos = function(callBack, id) {		// get the images contained in the album
		face.photos.length = 0;
		face.photoReadyCallBack = callBack;
		var id = id;
		var data = {};
		var token = localStorage.getItem("token");
		var images = "https://graph.facebook.com/v2.8/"+id+"?fields=photos%7Bimages%2Clink%2Cname%7D%2Cid&\
		access_token="+token+"";
		$.get(images, function(response) {
			for(var i in response.photos.data) {
				var imgLen = response.photos.data.length;
				for(var x in response.photos.data[i].images) {
					if(response.photos.data[i].images[x].height == 320) {
						data.thumb = response.photos.data[i].images[x].source
						
					}
					if(response.photos.data[i].images[x].height == 600) {
						data.large = response.photos.data[i].images[response.photos.data[i].images.length - response.photos.data[i].images.length + 1].source
					}
				}
				data.id = response.photos.data[i].id
				face.id.push(data.id);
				data.desc = response.photos.data[i].name;
				data.link = response.photos.data[i].link;
				data.id = response.photos.data[i].id

				face.saveObj(data);

			}
			//console.log(face.photos)
			face.photoReadyCallBack(face.photos)
		}, "json");
	},
	face.saveObj = function(data) {		// save each image and related data
		var photoObj = {
			thumb: data.thumb,
			large: data.large,
			link: data.link,
			desc: data.desc
		}
		photoObj = face.getLikes(photoObj,data.id)
		face.photos.push(photoObj)
	},
	face.getLikes = function(photoObj,id) {		// get the likes for the image
		photoObj.post = face.comments(photoObj, id)
		var token = localStorage.getItem("token");
		photoObj.id = id;
		var ID = localStorage.getItem("ID")
			var likes = "https://graph.facebook.com/v2.8/"+id+"?fields=likes.limit(1000)%7Bname%2Cpic%7D& \
			access_token="+token+"";
		$.get(likes, function(response) {
			//console.log(response)
			for(var i in response.likes.data) {
				//console.log(face.ID)
				//console.log(response.likes.data[i].id)
				if(response.likes.data[i].id == ID) {		// if the user has previously liked the image alter the like button
					localStorage.setItem('like', 'liked')
					$('#'+id+'').html("Liked").css("color", "blue");
					//console.log(response.likes.data[i].id, id)
				}
			}
			photoObj.likes = response.likes.data;
			photoObj.likeCount = photoObj.likes.length;
		}, "json");
		return photoObj
	},
	face.comments = function(photoObj, id) {		// get the comments for related image
		var postObj = {}
		var token = localStorage.getItem("token");
			var comments = "https://graph.facebook.com/v2.8/"+id+"?fields=comments&\
			access_token="+token+"";
		$.get(comments, function(response) {
			if(response.comments) {
				postObj.comments = response.comments
				postObj.comments.count = response.comments.data.length
			}
		}, "json");
		return postObj
	},
	face.postComment = function(comment, id) {		// comment on a picture
		//console.log(comment)
		var token = localStorage.getItem("token")
		FB.api(
			'/'+id+'/comments',
			'POST',
			{access_token: token, "message":""+comment+""},
			function(response) {
				//console.log(response)
			}
		);
	}